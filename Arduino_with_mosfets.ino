/*El siguiente código fue realizado para el control de un set de mosfets por medio de un arduino uno R3. 
 * 
 * El arduino tiene la facultad de recibir los siguientes comandos: 
 * 1) Sxxe@ 
 * S=Delimitador
 * xx= Puerto del set de mosfets(configurados del 1 al 12). 
 * e=Estado del pin (Encendido=1, Apagado=0, Para recibir una respuesta sobre el estado del pin=?)
 * @= Delimitador
 * 
 * Ejemplo: S100@ ----> Significa que el arduino debe apagar el puerto 10.
 *          S011@----> Significa que el arduino debe encender el puerto 1.
 *          S04?@----> Significa que el arduino debe dar respuesta sobre el estado del puerto 4 (ON o OFF dependiendo del estado).
 * 
 * 2) STATE
 * Este comando regresa el estado de cada uno de los pines en orden. 
 * Si el pin se encuentra apagado regresa un OFF y si se encuentra encendido regresa un ON.
 * 
 * Ejemplo de respuesta: 
 *         ON ON OFF ON OFF OFF OFF OFF OFF OFF OFF ON 
 * 
 * 3) RESET
 * 
 * El arduino fue configurado para reiniciar la entrada del radio (configurada por defecto como la número 1) 
 * para que se reinicie cada 24 horas durante 10 segundos. Se configuró un contador, que va ligado con la
 * frecuencia del oscilador, y puede ser reiniciada con el comando RESET.
 * 
 */

#include <EEPROM.h> //Incluir la memoria EEPROM
int memory;//Variable usada para leer la memoria EEPROM

bool RE = false;//Variable usada para el contador 
volatile unsigned int cuenta = 0; //Variable usada para el contador. Es el encargado de llevar la cuenta de los segundos transcurridos.

#define RADIO 2 //Declaración del pin real del arduino para el radio.

void setup() {
  Serial.begin(115200); //Configuración de los baudios.
  Serial.setTimeout(3000); //Tiempo que espera antes de limpiar el buffer.
  for(int p = 2; p <= 13; p++){  //Declaracion de todos los pines como salida menos el 0 y el 1 (TX y RX)
     pinMode(p,OUTPUT); //Configuración de los pines como salidas.
     memory= EEPROM.read(p); //Lectura de la EEPROM
     digitalWrite(p,memory); //Pone las salidas en alto o en bajo dependiendo de la EEPROM  
   } 

   digitalWrite(RADIO,HIGH); //El radio siempre empezará encendido  
   SREG = (SREG & 0b01111111); //Desabilitar interrupciones
   TIMSK2 = TIMSK2|0b00000001; //Habilita la interrupcion por desbordamiento
   TCCR2B = 0b00000111; //Configura preescala para que FT2 sea de 7812.5Hz
   SREG = (SREG & 0b01111111) | 0b10000000; //Habilitar interrupciones //Desabilitar interrupciones
}

void loop() { //Ciclo infinito
   char vector[5]; // vector que contiene la información recibida
   if (Serial.available()){
       String a=Serial.readStringUntil('\n'); //Lee la información hasta que encuentra un salto de linea.
       a.toCharArray(vector, 6); //Convierte la información en un vector de caracteres. 
       String str= String(vector); //Vuelve a convertir el vector en un String con los caracteres necesarios.
       int len=sizeof(vector); //Longitud del vector.

       if(vector[0]=='S' && vector[4]=='@' && len==5){ //Si tiene como caracter inicial S, como caracter final @ y tiene 5 caracteres procede a dar una respuesta
           char pin[2] = {vector[1], vector[2]};  //Adquisición del valor del pin que se desea configurar.
           int pinout = atoi(pin); //Conversión del pin a int
           pinout=pinout+1; //Se le suma 1 debido a la configuración de usuario.
           if(vector[3]=='1'){
              EEPROM.write(pinout, 1);  //Si el cuarto caracter es 1, pone la salida en alto, si es 0 la pone en bajo y si es ? da informacion sobre el estado del pin
              digitalWrite(pinout, 1); 
           }else if(vector[3]=='0'){
              EEPROM.write(pinout, 0);  
              digitalWrite(pinout, 0);
           }else if(vector[3]=='?'){
              int r=digitalRead(pinout);
              if(r==1){
                 Serial.println("ON");
          
              }else if(r==0){
                 Serial.println("OFF");
              }
           } 

       }else if(str=="RESET" && len==5){   //Reinicia el estado del contador del timer.
           cuenta=0;
           
        
       }else if(str=="STATE" && len==5){ // Información del estado de todos los pines
           for(int q = 2; q <= 13; q++){ 
             int w= digitalRead(q);
             if(w==1){
                Serial.print("ON ");
                 
             }else if(w==0){
                Serial.print("OFF ");
                
             }
           }
           Serial.print("\n"); 
       }
      

  }
}

ISR(TIMER2_OVF_vect){  //Configuración del timer.
    cuenta++;
    if(RE==false && cuenta > 2647059) { //2647059  24h  (error de 5.8ms)   tiempo encendido     0.03264 s cada ciclo
      digitalWrite(RADIO,LOW);
      cuenta=0;
      RE= true;
    }
    if(RE==true && cuenta>307){ // 307 10s (error de 20.4 ms) tiempo apagado
      digitalWrite(RADIO,HIGH);
      cuenta=0;
      RE= false;
    }
}
